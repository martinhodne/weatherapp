import 'package:flutter/material.dart';
import 'package:YrWeather/Forecast.dart';
import 'package:YrWeather/YrForecast.dart';
import 'package:intl/intl.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Yr testapp',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
      appBar: AppBar(
        title: Text("Yr testapp"),
      ),
      body: FutureBuilder<Forecast>(
  future: YrForecast.getWeatherDataWithLocation(language: "no"),
  builder: (context, snapshot) {
    if (snapshot.hasData) {
      List<ForecastData> fcdata = snapshot.data.forecast;
      var format = new DateFormat('dd:MM:yyyy HH:mm');
      return ListView.builder(
        itemCount: fcdata.length,
        itemBuilder: (context, index) => ListTile(
        title: Text(fcdata[index].place),
        subtitle: Text(
        "Type vær: " + fcdata[index].typeOfWeather + 
        "\nTemperatur:" + fcdata[index].temperature.toString() + 
        "\nFra: " + format.format(fcdata[index].dateAndTimeFrom) +
        "\nTil: " + format.format(fcdata[index].dateAndTimeTo) +
        "\nVindretning: " + fcdata[index].windDirection + 
        "\nVindstyrke: " + fcdata[index].windSpeed +
        "\n" + snapshot.data.creditText),
        leading: Image.network(fcdata[index].weatherPictureURL)
        ),
      );
    } else if (snapshot.hasError) {
      return Text("${snapshot.error}");
    }
    return CircularProgressIndicator();
  },
),
    ));
  }
}